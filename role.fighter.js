var roleFighter = {
    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room}});
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {

            var closestHostileStructureTower = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_TOWER);
                }
            });
            var closestHostileStructureSiteTower = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);
            var closestHostileStructureSpawn = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_SPAWN);
                }
            });
            var closestHostileStructureEx = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_EXTENSION);
                }
            });
            var closestHostileStructureContainer = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_CONTAINER);
                }
            });
            var closestHostileStructureStorage = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_STORAGE && ( _.sum(structure.store) == 0));
                }
            });
            var closestHostileStructure = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);
            var closestHostileStructureWall = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => { 
                    return (structure.structureType == STRUCTURE_RAMPART);
                }
            });
            var closestCreep = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if(closestHostileStructureTower){
                var target = closestHostileStructureTower;
            }else if(closestHostileStructureSpawn){
                var target = closestHostileStructureSpawn;
            }else if(closestCreep){
                var target = closestCreep;
            }else if(closestHostileStructureEx){
                var target = closestHostileStructureEx;
            }else if(closestHostileStructureContainer){
                var target = closestHostileStructureContainer;
            }else if(closestHostileStructureStorage){
                var target = closestHostileStructureStorage;
            }else if(closestHostileStructureWall){
                var target = closestHostileStructureWall;
            }
            if(closestHostileStructure){
                var target = closestHostileStructure;
            }
            if(target){
                if(creep.rangedAttack(target) == ERR_NOT_IN_RANGE || creep.attack(target) == ERR_NOT_IN_RANGE){
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffaa00'}})
                }
            }else{
                creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
            }
        }
    }
};

module.exports = roleFighter;