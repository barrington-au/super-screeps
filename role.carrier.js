var roleCarrier = {

    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        creeps = _.filter(Game.creeps, (creep) => creep.memory.role == profile.name && creep.memory.base_room == profile.base_room);
        if(creeps.length == 0){body=[CARRY,CARRY,MOVE,CARRY,CARRY,MOVE]}
        profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room}});
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {
            if(creep.memory.collecting && creep.carry.energy == 0) {
                creep.memory.collecting = false;
            }
            if(!creep.memory.collecting && creep.carry.energy == creep.carryCapacity) {
                creep.memory.collecting = true;
            }

            if(!creep.memory.collecting){
                /* Collect dropped energy first to reduce waste */
                const dropped_energy = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
                    filter: (resource) => { 
                        return resource.resourceType == RESOURCE_ENERGY && resource.amount > 50
                    }
                });
                /* Collect from storage boxes second as they live longer */
                const miner_storage = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => { 
                        return (structure.structureType == STRUCTURE_CONTAINER) && ( _.sum(structure.store) > 0);
                    }
                });
                /* Collect dropped energy first to reduce waste */
                if(dropped_energy) {
                    if(creep.pickup(dropped_energy, RESOURCE_ENERGY) ==  ERR_NOT_IN_RANGE) {
                        creep.moveTo(dropped_energy, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                /* Collect from storage boxes second as they live longer */
                }else if (miner_storage){
                    if(creep.withdraw(miner_storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(miner_storage, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }else{
                    /* Deliver this to the centeral storage box once we are full */
                    /* work out total room energy in sources */
                    var sources = creep.room.find(FIND_SOURCES);
                    var total_sources_energy_capacity = 0;
                    var total_sources_energy = 0;
                    for(var i in sources){
                        var total_sources_energy = total_sources_energy + sources[i].energy;
                        var total_sources_energy_capacity = total_sources_energy_capacity + sources[i].energyCapacity;
                    }

                    /* if there is some energry missing in the spawn or the extensions go fill them */
                    if(creep.room.energyAvailable != creep.room.energyCapacityAvailable){
                        var sources = creep.room.find(FIND_STRUCTURES, {
                            filter: (structure) => {
                                return (structure.structureType == STRUCTURE_STORAGE);
                            }
                        });
                        var maxAmount = -1;
                        var maxSource = null;
                        var maxRange = 100;
                        for (var i = 0; i < sources.length; i++) {
                            if (sources[i].store[RESOURCE_ENERGY] >= maxAmount) {
                                var range = creep.pos.getRangeTo(sources[i]);
                                if (sources[i].store[RESOURCE_ENERGY] > maxAmount || range < maxRange) {
                                    maxAmount = sources[i].store[RESOURCE_ENERGY];
                                    maxSource = sources[i];
                                    maxRange = range;
                                }
                            }
                        }
                        if(maxSource) {
                            if(creep.withdraw(maxSource, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                                creep.moveTo(maxSource, {visualizePathStyle: {stroke: '#ffffff'}});
                            }
                        }
                    }
                }
            }else{
                /* Prioritise the empty spawn or extension so that we always can build creeps */
                if(creep.room.energyAvailable != creep.room.energyCapacityAvailable){
                    var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return ( 
                                structure.structureType == STRUCTURE_SPAWN ||
                                structure.structureType == STRUCTURE_EXTENSION) && ( structure.energy < structure.energyCapacity );
                        }
                    });
                }else{
                    /* Otherwise store energy up */
                    var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return (structure.structureType == STRUCTURE_STORAGE) && ( _.sum(structure.store) < (structure.storeCapacity));
                        }
                    });
                }
                if(target) {
                    if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }
        }
    }
};

module.exports = roleCarrier;