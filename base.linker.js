var baseLinker = {
    run: function(){
        var link = Game.rooms['W31N7'].find(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}});
        if(link[2].energy == 0){
            if(link[0].energy == link[0].energyCapacity){
                link[0].transferEnergy(link[2]);
            }else if(link[1].energy == link[1].energyCapacity){
                link[1].transferEnergy(link[2]);
            }
        }
        var link = Game.rooms['W31N6'].find(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}});
        if(link[1].energy == 0){
            if(link[0].energy == link[0].energyCapacity){
                link[0].transferEnergy(link[1]);
            }
        }
        var link = Game.rooms['W31N9'].find(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}});
        if(link[0].energy == 0){
            if(link[1].energy == link[1].energyCapacity){
                link[1].transferEnergy(link[0]);
            }
        }

        for(var f in Game.rooms){
            room = Game.rooms[f]
            var storage = room.find(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_STORAGE)}});
            // console.log(room)
            if(storage[0]){
                // console.log(storage)
                var storage_link = storage[0].pos.findClosestByRange(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}}, 3)
                // console.log(storage_link)
                if(storage_link != "" && storage_link != null){
                    var sources = room.find(FIND_SOURCES)
                    // console.log(sources)
                    if(sources != ""){
                        for(var i in sources){
                            var source = sources[i]
                            // console.log(source)
                            var source_link = source.pos.findClosestByRange(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}}, 3)
                            if(source_link){
                                // console.log(source_link)
                                if(source_link.energy == source_link.energyCapacity){
                                    source_link.transferEnergy(storage_link);
                                    // console.log("TRASFERTING - "+room)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // console.log("-----------------------------------------\n\n\n")
};

module.exports = baseLinker;
