var roleUpgrader = {

    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room}});
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {
            if(creep.memory.upgrading && creep.carry.energy == 0) {
                creep.memory.upgrading = false;
            }
            if(!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
                creep.memory.upgrading = true;
            }

            if(creep.memory.upgrading) {
                if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}});
                }
            } else {
                /* Collect dropped energy first to reduce waste */
                const dropped_energy = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
                    filter: (resource) => { 
                        return resource.resourceType == RESOURCE_ENERGY && resource.amount > 50
                    }
                });
                /* Collect from storage boxes second as they live longer */
                const miner_storage = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => { 
                        return (structure.structureType == STRUCTURE_CONTAINER) && ( _.sum(structure.store) > 0);
                    }
                });
                var storage = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_STORAGE && ( _.sum(structure.store) > 0));
                    }
                });
                if(storage[0]) {
                    if(creep.withdraw(storage[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(storage[0], {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }else if(miner_storage){
                    if(creep.withdraw(miner_storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(miner_storage, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }else if(dropped_energy){
                    if(creep.pickup(dropped_energy, RESOURCE_ENERGY) ==  ERR_NOT_IN_RANGE) {
                        creep.moveTo(dropped_energy, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }
        }
    }
};

module.exports = roleUpgrader;