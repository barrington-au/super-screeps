var roleHauler = {

    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        creeps = _.filter(Game.creeps, (creep) => creep.memory.role == profile.name && creep.memory.base_room == profile.base_room);
        console.log(profile.spawner)
        console.log(profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room,depot_room:profile.depot_room}}))
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.carry.energy == 0) {
            creep.memory.collecting = false;
            creep.memory.active_room = creep.memory.base_room;
        }else if(creep.carry.energy == creep.carryCapacity){
            creep.memory.collecting = true;
            creep.memory.active_room = creep.memory.depot_room;
        }

        if (creep.room.name != creep.memory.active_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.active_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        }else{
            
            if(!creep.memory.collecting){
                /* Collect dropped energy first to reduce waste */
                const dropped_energy = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
                    filter: (resource) => { 
                        return resource.resourceType == RESOURCE_ENERGY
                    }
                });
                /* Collect from container boxes second as they live longer */
                const container = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => { 
                        return (structure.structureType == STRUCTURE_CONTAINER) && ( _.sum(structure.store) > 0);
                    }
                });
                /* Collect from storage boxes second as they live longer */
                const storage = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                    filter: (structure) => { 
                        return (structure.structureType == STRUCTURE_STORAGE) && ( _.sum(structure.store) > 0 );
                    }
                });

                /* Collect dropped energy first to reduce waste */
                if(storage){
                    if(creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(storage, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }else 
                if(dropped_energy) {
                    if(creep.pickup(dropped_energy, RESOURCE_ENERGY) ==  ERR_NOT_IN_RANGE) {
                        creep.moveTo(dropped_energy, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                /* Collect from storage boxes second as they live longer */
                }else if (container){
                    if(creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(container, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }else{
                var target = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_STORAGE);
                    }
                });
                if(target) {
                    if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }
        }
    }
};

module.exports = roleHauler;