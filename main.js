var roleLoader = require('role.loader');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleCarrier = require('role.carrier');
var roleMiner = require('role.miner');
var roleFighter = require('role.fighter');
var roleExcavator = require('role.excavator');
var roleHauler = require('role.hauler');
var roleClaimer = require('role.claimer');
var roleLinker = require('role.linker');
var roleHealer = require('role.healer');
var structureTower = require('structure.tower');
var structureSpawner = require('structure.spawner');
var baseLinker = require('base.linker');

module.exports.loop = function () {
    baseLinker.run();

    var population = [
        // TOP ROOM, SINGLE POWER SOURCE
        {
            'ROOM':'W31N9',
            'SPAWN_ROOM':'W31N9',
            'POPULATION':{
                /** SPAWN ACTIVE MAIN ROOM */
                '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:1},
                '2':{name:'upgrader',total:0,num:1,body:[WORK,CARRY,MOVE],cap:10,role:'roleUpgrader'},
                '3':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLoader'},
                '4':{name:'carrier',total:0,num:2,body:[CARRY,CARRY,CARRY,MOVE],cap:2,role:'roleCarrier'},
                // '5':{name:'builder',total:0,num:1,body:[WORK,CARRY,CARRY,MOVE],cap:5,role:'roleBuilder'},
                '6':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:1,role:'roleLinker'},
            },
        },
        // MAIN ROOM TWO SPAWNS TWO SOURCES, 2 EXTERNAL MINING ROOMS
        {
            'ROOM':'W31N7',
            'SPAWN_ROOM':'W31N7',
            'POPULATION':{
                /** SPAWN ACTIVE MAIN ROOM */
                '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:0},
                '1':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:1,target_link:1},
                '2':{name:'upgrader',total:0,num:1,body:[WORK,CARRY,MOVE],cap:1,role:'roleUpgrader'},
                '3':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:3,role:'roleLoader'},
                '4':{name:'carrier',total:0,num:3,body:[CARRY,CARRY,CARRY,MOVE],cap:6,role:'roleCarrier'},
                '5':{name:'builder',total:0,num:2,body:[WORK,CARRY,CARRY,MOVE],cap:10,role:'roleBuilder'},
                '6':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLinker'},
                // '7':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N6'},
            },
        },
        {
            'ROOM':'W29N8',
            'SPAWN_ROOM':'W31N7',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'hauler',total:0,num:1,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N7'},
            },
        },
        {
            'ROOM':'W29N7',
            'SPAWN_ROOM':'W31N7',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'hauler',total:0,num:3,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N7'},
                '3':{name:'fighter',total:0,num:1,body:[TOUGH,RANGED_ATTACK,MOVE,MOVE],cap:3,role:'roleFighter'},
                '4':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,MOVE],cap:1,role:'roleClaimer'},
                '5':{name:'healer',total:0,num:1,body:[TOUGH,HEAL,MOVE,MOVE],cap:3,role:'roleHealer'},
            },
        },
        {
            'ROOM':'W29N6',
            'SPAWN_ROOM':'W31N7',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'hauler',total:0,num:3,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N7'},
                '3':{name:'fighter',total:0,num:1,body:[RANGED_ATTACK,TOUGH,MOVE,MOVE],cap:5,role:'roleFighter'},
                '4':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            'ROOM':'W31N8',
            'SPAWN_ROOM':'W31N7',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'hauler',total:0,num:3,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N9'},
                '3':{name:'fighter',total:0,num:1,body:[RANGED_ATTACK,TOUGH,MOVE,MOVE],cap:5,role:'roleFighter'},
                '4':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        // SECOND ROOM, 2 SOURCES
        {
            'ROOM':'W31N6',
            'SPAWN_ROOM':'W31N6',
            'POPULATION':{
                '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:0},
                '1':{name:'upgrader',total:0,num:1,body:[WORK,CARRY,MOVE],cap:6,role:'roleUpgrader'},
                '2':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:8,role:'roleLoader'},
                '3':{name:'carrier',total:0,num:2,body:[CARRY,CARRY,CARRY,MOVE],cap:4,role:'roleCarrier'},
                // '4':{name:'builder',total:0,num:1,body:[WORK,CARRY,CARRY,MOVE],cap:5,role:'roleBuilder'},
                '5':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLinker'},
                // '6':{name:'hauler',total:0,num:1,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
            },
        },
        // FOURTH ROOM, 2 SOURCES, 2 EXTERNAL ROOMS
        {
            'ROOM':'W31N3',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:1},
                '1':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:1,target_link:0},
                '2':{name:'upgrader',total:0,num:3,body:[WORK,CARRY,MOVE],cap:10,role:'roleUpgrader'},
                '3':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLoader'},
                '4':{name:'carrier',total:0,num:3,body:[CARRY,CARRY,CARRY,MOVE],cap:6,role:'roleCarrier'},
                '5':{name:'builder',total:0,num:1,body:[WORK,CARRY,CARRY,MOVE],cap:5,role:'roleBuilder'},
                '6':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:1,role:'roleLinker'},
            },
        },
        {
            'ROOM':'W31N4',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
                '3':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE],cap:1,role:'roleClaimer'},
                '4':{name:'fighter',total:0,num:1,body:[RANGED_ATTACK,TOUGH,MOVE,MOVE],cap:5,role:'roleFighter'},
            },
        },
        {
            'ROOM':'W32N3',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
                '3':{name:'fighter',total:0,num:1,body:[TOUGH,RANGED_ATTACK,MOVE,MOVE],cap:3,role:'roleFighter'},
                '4':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            'ROOM':'W31N2',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
                '2':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            'ROOM':'W33N3',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
                '2':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            'ROOM':'W32N4',
            'SPAWN_ROOM':'W31N3',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W31N3'},
                '2':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            // Fith Room
            'ROOM':'W32N2',
            'SPAWN_ROOM':'W32N2',
            'POPULATION':{
                '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:0},
                '1':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:1,target_link:1},
                '2':{name:'upgrader',total:0,num:1,body:[WORK,CARRY,MOVE],cap:1,role:'roleUpgrader'},
                '3':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:3,role:'roleLoader'},
                '4':{name:'carrier',total:0,num:2,body:[CARRY,CARRY,CARRY,MOVE],cap:6,role:'roleCarrier'},
                // '5':{name:'builder',total:0,num:2,body:[WORK,CARRY,CARRY,MOVE],cap:10,role:'roleBuilder'},
                '6':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLinker'},
            },
        },
        {
            'ROOM':'W33N2',
            'SPAWN_ROOM':'W32N2',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'hauler',total:0,num:2,body:[CARRY,CARRY,MOVE,MOVE],cap:10,role:'roleHauler',depot_room:'W32N2'},
                '2':{name:'fighter',total:0,num:1,body:[RANGED_ATTACK,TOUGH,MOVE,MOVE],cap:5,role:'roleFighter'},
                '3':{name:'claimer',total:0,num:1,body:[CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE],cap:1,role:'roleClaimer'},
            },
        },
        {
            // Sixth room
            'ROOM':'W32N11',
            'SPAWN_ROOM':'W31N9',
            'POPULATION':{
                '0':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:0},
                '1':{name:'excavator',total:0,num:1,body:[WORK,WORK,MOVE],cap:5,role:'roleExcavator',target_source:1},
                '2':{name:'upgrader',total:0,num:2,body:[WORK,CARRY,MOVE],cap:11,role:'roleUpgrader'},
                '4':{name:'fighter',total:0,num:1,body:[TOUGH,RANGED_ATTACK,MOVE,MOVE],cap:10,role:'roleFighter'},
                '5':{name:'healer',total:0,num:1,body:[TOUGH,HEAL,MOVE,MOVE],cap:5,role:'roleHealer'},
                // '0':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:0,target_link:1},
                // '1':{name:'miner',total:0,num:1,body:[WORK,WORK,MOVE,CARRY],cap:5,role:'roleMiner',target_source:1,target_link:0},
                // '2':{name:'upgrader',total:0,num:2,body:[WORK,CARRY,MOVE],cap:10,role:'roleUpgrader'},
                '3':{name:'loader',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:2,role:'roleLoader'},
                // '4':{name:'carrier',total:0,num:1,body:[CARRY,CARRY,CARRY,MOVE],cap:6,role:'roleCarrier'},
                '6':{name:'builder',total:0,num:2,body:[WORK,CARRY,CARRY,MOVE],cap:10,role:'roleBuilder'},
                // '6':{name:'linker',total:0,num:1,body:[CARRY,CARRY,MOVE],cap:1,role:'roleLinker'},
            },
        },
    ]
    var contributing_rooms = [
        // 'W31N9',
        // 'W31N7',
        // 'W31N6',
    ];
    var war_population = {
        // 'W23N11':{
        //     '0':{name:'fighter',total:0,num:4,body:[RANGED_ATTACK,TOUGH,MOVE,MOVE],cap:15,role:'roleFighter'},
        //     '1':{name:'healer',total:0,num:4,body:[TOUGH,HEAL,MOVE,MOVE],cap:15,role:'roleHealer'},
        // },
  
    };


    /* Purge memory  */
    for(var i in Memory.creeps) {
        if(!Game.creeps[i]) {
            delete Memory.creeps[i];
        }
    }

    for(i in Game.rooms){
        // var room_name = Game.rooms[i].name
        // var room_progress = parseFloat((Game.rooms[room_name].controller.progress / Game.rooms[room_name].controller.progressTotal) * 100).toFixed(2);
        // console.log('Spawn Energy: ' + Game.rooms[room_name].energyAvailable + '/' + Game.rooms[room_name].energyCapacityAvailable + ', Storage Energy: ' + Game.rooms[room_name].storage.store[RESOURCE_ENERGY] + '/' + Game.rooms[room_name].storage.storeCapacity + ', Room Progress: Level (' + Game.rooms[room_name].controller.level + ') '+ room_progress + '%\n');

        /* TOWER TICKER */
        towers = Game.rooms[i].find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_TOWER }
        })
        _.forEach(towers, function(tower){
            structureTower.run(tower);
        })
    }
    for(var i in Game.spawns){
        if(Game.spawns[i].spawning){
            var spawningCreep = Game.creeps[Game.spawns[i].spawning.name];
            Game.spawns[i].room.visual.text(
                '🛠️' + spawningCreep.memory.role,
                Game.spawns[i].pos.x + 2, 
                Game.spawns[i].pos.y + 3, 
                {align: 'left', opacity: 0.8}
            );
        }
    }

    structureSpawner.spawn(population);
    structureSpawner.war(war_population, contributing_rooms);

    /* CREEPS TICKER */
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.ticksToLive < 100){
            // console.log(name + ' is about to die in ' + creep.ticksToLive);
            // var spawn = Game.rooms[creep.room.name].find(FIND_MY_STRUCTURES, {
            //     filter: (structure) => {
            //         return (structure.structureType == STRUCTURE_SPAWN);
            //     }
            // });
            // if(spawn.length != 0){
            //     if(spawn[0].renewCreep(creep) != 0){
            //         console.log(name + ' is moving to spawn');
            //         creep.moveTo(spawn[0]);
            //     }
            // }
            // continue
        }
        if(creep.memory.role == 'fighter') {
            roleFighter.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'loader') {
            roleLoader.run(creep);
        }
        if(creep.memory.role == 'carrier') {
            roleCarrier.run(creep);
        }
        if(creep.memory.role == 'miner') {
            roleMiner.run(creep);
        }
        if(creep.memory.role == 'excavator'){
            roleExcavator.run(creep);
        }
        if(creep.memory.role == 'hauler'){
            roleHauler.run(creep);
        }
        if(creep.memory.role == 'claimer'){
            roleClaimer.run(creep);
        }
        if(creep.memory.role == 'linker'){
            roleLinker.run(creep);
        }
        if(creep.memory.role == 'healer'){
            roleHealer.run(creep);
        }
    }
}

/**if(!creep.memory.path) {
    creep.memory.path = creep.pos.findPathTo(target);
}
creep.moveByPath(creep.memory.path); */