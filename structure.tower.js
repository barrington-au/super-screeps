var structureTower = {
    run: function(tower) {
        var friends = ['pbootly','Hairy','slyfoxuk'];
        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if(closestHostile) {
            // if(freinds.indexOf(closestHostile.owner) != 0){
                console.log('[WARN] Base Under Attack in ' + tower.room + '!');
                tower.attack(closestHostile);
            // }
        }else{
            var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => { return ( (structure.structureType == STRUCTURE_WALL && structure.hits < 50 ) || (structure.structureType == STRUCTURE_RAMPART && structure.hits < 5000) || (structure.structureType != STRUCTURE_WALL && structure.structureType != STRUCTURE_RAMPART && structure.hits < structure.hitsMax))}
            });
            if(closestDamagedStructure) {
                tower.repair(closestDamagedStructure);
            }
            var closetestDamagedCreep = tower.pos.findClosestByRange(FIND_MY_CREEPS, {
                filter: (creep) => { return (creep.hits != creep.hitsMax)}
            })
            if(closetestDamagedCreep) {
                tower.heal(closetestDamagedCreep);
            }
        }
    }
};

module.exports = structureTower;