var roleLinker = {

    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room}});
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {
            if(creep.memory.collecting && creep.carry.energy == 0) {
                creep.memory.collecting = false;
            }
            if(!creep.memory.collecting && creep.carry.energy != 0) {
                creep.memory.collecting = true;
            }

            if(!creep.memory.collecting){
                /* Collect from storage boxes second as they live longer */
                const link = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => { 
                        return (structure.structureType == STRUCTURE_LINK);
                    }
                });
                /* Collect dropped energy first to reduce waste */
                if(link) {
                    if(creep.withdraw(link, RESOURCE_ENERGY) ==  ERR_NOT_IN_RANGE) {
                        creep.moveTo(link, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }else{
                var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_STORAGE) && ( _.sum(structure.store) < (structure.storeCapacity));
                    }
                });
                if(target) {
                    if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }
        }
    }
};

module.exports = roleLinker;