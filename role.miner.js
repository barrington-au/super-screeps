var roleMiner = {
    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        profile.spawner.spawnCreep(body, newName, {
            memory: {
                role: profile.name,
                target_source:profile.target_source,
                target_link:profile.target_link,
                base_room:profile.base_room,
            }
        });
    },
 
    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {
            var link = creep.pos.findClosestByRange(FIND_STRUCTURES, {filter: (structure) => { return (structure.structureType == STRUCTURE_LINK)}},3);
            my_link = link;
            if(my_link && (creep.carry.energy >= (creep.carryCapacity -10))){
                if(my_link.energy != my_link.energyCapacity){
                    if(creep.transfer(my_link, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(my_link.pos, {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
            }else{
                var sources = creep.room.find(FIND_SOURCES);
                if(creep.harvest(sources[creep.memory.target_source]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(sources[creep.memory.target_source], {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
        }
    }
};

module.exports = roleMiner;