var baseCreep = {
    getClosestEnergyStorage: function(creep){
        return creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_STORAGE);
            }
        });
    },

    getClosestDroppedEnergy: function(creep){
        return creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
            filter: (resource) => { 
                return resource.resourceType == RESOURCE_ENERGY && resource.amount > 50;
            }
        });
    },

    getClosestContainer: function(creep){
        /* Collect from storage boxes second as they live longer */
        return creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => { 
                return (structure.structureType == STRUCTURE_CONTAINER) && ( _.sum(structure.store) > 0);
            }
        });
    },

    getClosestEnergy: function(creep){
        var storage = this.getClosestDroppedEnergy(creep);
        var container = this.getClosestContainer(creep);
        var dropped_energy = this.getClosestDroppedEnergy(creep);
        
        if(dropped_energy){
            return dropped_energy;
        }else if(container){
            return container;
        }else if(storage){
            return storage;
        }else{
            return undefined;
        }
    }
}

module.exports = baseCreep;