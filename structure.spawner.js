var roleExcavator = require('role.excavator');
var roleHauler = require('role.hauler');

var structureSpawner = {
    
    war: function(war_population, contributing_rooms) {
        var status = '|--------------------------------------------------------------------------------------|\n';
        for(let i in war_population){
            let room = war_population[i];
            
            status += 'ENEMY ROOM: ' + i + '\n';

            for(var f in contributing_rooms){
                var spawn = Game.rooms[contributing_rooms[f]].find(FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_SPAWN);
                    }
                });
                for(let a in room){
                    let creeper = room[a];
                    
                    if(creeper.name == 'hauler'){
                        creeps = _.filter(Game.creeps, (creep) => creep.memory.role == creeper.name && creep.memory.base_room == i && creep.memory.depot_room == creeper.depot_room);
                        if(creeper.num != 0){status += ' - ' +contributing_rooms[f] + creeper.name + ': ' + creeps.length + '/' + creeper.num + ' '+creeper.depot_room+ '\n';}
                    }else if(creeper.name == 'miner' || creeper.name == 'excavator'){
                        creeps = _.filter(Game.creeps, (creep) => creep.memory.role == creeper.name && creep.memory.base_room == i && creep.memory.target_source == creeper.target_source);
                        if(creeper.num != 0){status += ' - ' +contributing_rooms[f] + creeper.name + ': ' + creeps.length + '/' + creeper.num + ' '+creeper.target_source+ '\n';}
                    }else{
                        creeps = _.filter(Game.creeps, (creep) => creep.memory.role == creeper.name && creep.memory.base_room == i);
                        if(creeper.num != 0){status += ' - ' +contributing_rooms[f] + creeper.name + ': ' + creeps.length + '/' + creeper.num + '\n';}
                    }
                    /* append needed info */
                    creeper.base_room = i;
                    creeper.energy_cost = 0;
                    for(var part in creeper.body){
                        creeper.energy_cost += BODYPART_COST[creeper.body[part]];
                    }
                    creeper.spawn_room = contributing_rooms[f];
                    if(contributing_rooms[f] == 'W31N7'){
                        if(spawn[0].spawning){
                            creeper.spawner = spawn[1];
                        }else{
                            creeper.spawner = spawn[0];
                        }
                    }else{
                        creeper.spawner = spawn[0];
                    }
                    if(creeps.length < creeper.num) {
                        role = require('role.'+creeper.name);
                        role.spawn(creeper);
                    }
                }
            }
            status += '|--------------------------------------------------------------------------------------|\n';
        }
        // console.log(status);
    },
    spawn: function(population) {
        var status = ''
        for(let i in population){
            var room_name = population[i]['ROOM']
            var spawn_room = population[i]['SPAWN_ROOM']
            var room_creeps = population[i]['POPULATION']
            var spawn = Game.rooms[spawn_room].find(FIND_MY_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_SPAWN && !structure.spawning);
                }
            });
            status += '[ROOM] ' + room_name + '\n'
            for(let f in room_creeps){

                // Build the creeper object to be passed to the creeper spawn function
                let creeper = room_creeps[f];
                creeper.base_room = room_name;
                creeper.energy_cost = 0;
                for(var part in creeper.body){
                    creeper.energy_cost += BODYPART_COST[creeper.body[part]];
                }
                creeper.spawn_room = spawn_room;
                creeper.spawner = spawn[0];

                /* Only if it is a miner is it different for now */
                if(creeper.role == 'roleMiner' || creeper.role == 'roleExcavator'){
                    creeps = _.filter(Game.creeps, (creep) => (creep.memory.role == creeper.name && creep.memory.base_room == room_name && creep.memory.target_source == creeper.target_source) || creep.ticksToLive < 50);
                }else{
                    creeps = _.filter(Game.creeps, (creep) => (creep.memory.role == creeper.name && creep.memory.base_room == room_name) || creep.ticksToLive < 50 );
                }
                // status += ' - ' + creeper.name + ': ' + creeps.length + '/' + creeper.num + '\n'
                if(creeps.length < creeper.num) {
                    role = require('role.'+creeper.name);
                    status += ' - [SPAWNING] ' + creeper.name + '\n'
                    if(spawn[0]){
                        output = role.spawn(creeper);
                        status += creeper + '\n'
                    }
                }
            }
        }
        console.log(status)
    },
};

module.exports = structureSpawner;