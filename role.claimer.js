var roleClaimer = {
    spawn: function(profile){
        x = Math.floor((Game.rooms[profile.spawn_room].energyCapacityAvailable) / profile.energy_cost);
        if( x > profile.cap){x=profile.cap}
        var body = [];
        for(var i = 0; i < x; i++) {
            body = body.concat(profile.body);
        }
        var newName = profile.name + Game.time;
        profile.spawner.spawnCreep(body, newName, {memory: {role: profile.name,base_room:profile.base_room}});
    },

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name != creep.memory.base_room) {
            creep.moveTo(new RoomPosition(19, 45, creep.memory.base_room),{visualizePathStyle: {stroke: '#ffaa00'}})
        } else {
            if(!creep.room.controller.my){
                if(creep.room.name == 'W32N11'){
                    creep.claimController(creep.room.controller)
                }
                if(!creep.room.controller.my && creep.room.controller.owner != undefined){
                    if(creep.attackController(creep.room.controller) == ERR_NOT_IN_RANGE){
                        creep.moveTo(creep.room.controller);
                    }
                }else{
                    if(creep.reserveController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(creep.room.controller);
                    }else{
                        creep.signController(creep.room.controller, "General Barrington's room");
                    }
                }
            }
        }
    }
};

module.exports = roleClaimer;